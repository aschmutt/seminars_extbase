<?php
namespace Schmutt\SeminarsExtbase\Controller;

/**
 * This file is part of the "seminars_extbase" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Controller of news records
 *
 */
class SeminarController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var \Schmutt\SeminarsExtbase\Domain\Repository\EventRepository
     */
    protected $eventRepository;

    /**
     * @var \Schmutt\SeminarsExtbase\Domain\Repository\SpeakerRepository
     */
    protected $speakerRepository;

    /**
     * @param \Schmutt\SeminarsExtbase\Domain\Repository\EventRepository $eventRepository
     */
    public function injectEventRepository(\Schmutt\SeminarsExtbase\Domain\Repository\EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }


    /**
     * @param \Schmutt\SeminarsExtbase\Domain\Repository\SpeakerRepository $speakerRepository
     */
    public function injectSpeakerRepository(\Schmutt\SeminarsExtbase\Domain\Repository\SpeakerRepository $speakerRepository)
    {
        $this->speakerRepository = $speakerRepository;
    }


    /**
     * action list
     *
     * @return void
     */
    public function listAction() {

        $events = $this->eventRepository->getEventList();
        $this->view->assign('events', $events);


        //tt_content
        //$data = $this->configurationManager->getContentObject()->data;
        //$this->view->assign('data',$data);

        //$imgPath = ExtensionManagementUtility::siteRelPath('seminars_extbase') . 'Resources/Public/images';
        //$this->view->assign('imgPath', $imgPath);
    }

    /**
     * action show
     *
     * @param \Schmutt\SeminarsExtbase\Domain\Model\Event $event
     *
     * @return void
     */
    public function showAction($event = NULL) {

        if ($event == NULL) {
            //$productUids = $this->settings['produktauswahl'];

        } else {

            $this->view->assign('event',$event);
        }

    }

}
