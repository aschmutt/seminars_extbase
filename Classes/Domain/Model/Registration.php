<?php
namespace Schmutt\SeminarsExtbase\Domain\Model;

/**
 * This file is part of the "seminars_extbase" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */


/**
 * Registration
 */
class Registration extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

/***************************************************
 * Database Fields
 ***************************************************/

    /**
     * @var string
     */
    protected $title;

    /**
     * @var int
     */
    protected $user;

    /**
     * @var int
     */
    protected $seminar;

    /**
     * @var int
     */
    protected $registrationQueue;

    /**
     * @var string
     */
    protected $price;

    /**
     * @var int
     */
    protected $seats;

    /**
     * @var bool
     */
    protected $registeredThemselves;

    /**
     * @var double
     */
    protected $totalPrice;

    /**
     * @var int
     */
    protected $currency;

    /**
     * @var bool
     */
    protected $includingTax;

    /**
     * @var string
     */
    protected $attendeesNames;

    /**
     * @var int
     */
    protected $additionalPersons;

    /**
     * @var int
     */
    protected $datepaid;

    /**
     * @var int
     */
    protected $methodOfPayment;

    /**
     * @var string
     */
    protected $bankCode;

    /**
     * @var string
     */
    protected $bankName;

    /**
     * @var string
     */
    protected $accountOwner;

    /**
     * @var string
     */
    protected $company;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $gender;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $zip;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $country;

    /**
     * @var string
     */
    protected $telephone;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var int
     */
    protected $beenThere;

    /**
     * @var string
     */
    protected $interests;

    /**
     * @var string
     */
    protected $expectations;

    /**
     * @var string
     */
    protected $backgroundKnowledge;

    /**
     * @var string
     */
    protected $accommodation;

    /**
     * @var int
     */
    protected $lodgings;

    /**
     * @var string
     */
    protected $food;

    /**
     * @var int
     */
    protected $foods;

    /**
     * @var string
     */
    protected $knownFrom;

    /**
     * @var string
     */
    protected $notes;

    /**
     * @var int
     */
    protected $kids;

    /**
     * @var int
     */
    protected $checkboxes;

    /***************************************************
     * Getters and Setters
     ***************************************************/

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getSeminar()
    {
        return $this->seminar;
    }

    /**
     * @param int $seminar
     */
    public function setSeminar($seminar)
    {
        $this->seminar = $seminar;
    }

    /**
     * @return int
     */
    public function getRegistrationQueue()
    {
        return $this->registrationQueue;
    }

    /**
     * @param int $registrationQueue
     */
    public function setRegistrationQueue($registrationQueue)
    {
        $this->registrationQueue = $registrationQueue;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param int $seats
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    /**
     * @return bool
     */
    public function isRegisteredThemselves()
    {
        return $this->registeredThemselves;
    }

    /**
     * @param bool $registeredThemselves
     */
    public function setRegisteredThemselves($registeredThemselves)
    {
        $this->registeredThemselves = $registeredThemselves;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param int $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return bool
     */
    public function isIncludingTax()
    {
        return $this->includingTax;
    }

    /**
     * @param bool $includingTax
     */
    public function setIncludingTax($includingTax)
    {
        $this->includingTax = $includingTax;
    }

    /**
     * @return string
     */
    public function getAttendeesNames()
    {
        return $this->attendeesNames;
    }

    /**
     * @param string $attendeesNames
     */
    public function setAttendeesNames($attendeesNames)
    {
        $this->attendeesNames = $attendeesNames;
    }

    /**
     * @return int
     */
    public function getAdditionalPersons()
    {
        return $this->additionalPersons;
    }

    /**
     * @param int $additionalPersons
     */
    public function setAdditionalPersons($additionalPersons)
    {
        $this->additionalPersons = $additionalPersons;
    }

    /**
     * @return int
     */
    public function getDatepaid()
    {
        return $this->datepaid;
    }

    /**
     * @param int $datepaid
     */
    public function setDatepaid($datepaid)
    {
        $this->datepaid = $datepaid;
    }

    /**
     * @return int
     */
    public function getMethodOfPayment()
    {
        return $this->methodOfPayment;
    }

    /**
     * @param int $methodOfPayment
     */
    public function setMethodOfPayment($methodOfPayment)
    {
        $this->methodOfPayment = $methodOfPayment;
    }

    /**
     * @return string
     */
    public function getBankCode()
    {
        return $this->bankCode;
    }

    /**
     * @param string $bankCode
     */
    public function setBankCode($bankCode)
    {
        $this->bankCode = $bankCode;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getAccountOwner()
    {
        return $this->accountOwner;
    }

    /**
     * @param string $accountOwner
     */
    public function setAccountOwner($accountOwner)
    {
        $this->accountOwner = $accountOwner;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getBeenThere()
    {
        return $this->beenThere;
    }

    /**
     * @param int $beenThere
     */
    public function setBeenThere($beenThere)
    {
        $this->beenThere = $beenThere;
    }

    /**
     * @return string
     */
    public function getInterests()
    {
        return $this->interests;
    }

    /**
     * @param string $interests
     */
    public function setInterests($interests)
    {
        $this->interests = $interests;
    }

    /**
     * @return string
     */
    public function getExpectations()
    {
        return $this->expectations;
    }

    /**
     * @param string $expectations
     */
    public function setExpectations($expectations)
    {
        $this->expectations = $expectations;
    }

    /**
     * @return string
     */
    public function getBackgroundKnowledge()
    {
        return $this->backgroundKnowledge;
    }

    /**
     * @param string $backgroundKnowledge
     */
    public function setBackgroundKnowledge($backgroundKnowledge)
    {
        $this->backgroundKnowledge = $backgroundKnowledge;
    }

    /**
     * @return string
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * @param string $accommodation
     */
    public function setAccommodation($accommodation)
    {
        $this->accommodation = $accommodation;
    }

    /**
     * @return int
     */
    public function getLodgings()
    {
        return $this->lodgings;
    }

    /**
     * @param int $lodgings
     */
    public function setLodgings($lodgings)
    {
        $this->lodgings = $lodgings;
    }

    /**
     * @return string
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param string $food
     */
    public function setFood($food)
    {
        $this->food = $food;
    }

    /**
     * @return int
     */
    public function getFoods()
    {
        return $this->foods;
    }

    /**
     * @param int $foods
     */
    public function setFoods($foods)
    {
        $this->foods = $foods;
    }

    /**
     * @return string
     */
    public function getKnownFrom()
    {
        return $this->knownFrom;
    }

    /**
     * @param string $knownFrom
     */
    public function setKnownFrom($knownFrom)
    {
        $this->knownFrom = $knownFrom;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return int
     */
    public function getKids()
    {
        return $this->kids;
    }

    /**
     * @param int $kids
     */
    public function setKids($kids)
    {
        $this->kids = $kids;
    }

    /**
     * @return int
     */
    public function getCheckboxes()
    {
        return $this->checkboxes;
    }

    /**
     * @param int $checkboxes
     */
    public function setCheckboxes($checkboxes)
    {
        $this->checkboxes = $checkboxes;
    }


}
