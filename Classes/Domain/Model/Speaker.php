<?php
namespace Schmutt\SeminarsExtbase\Domain\Model;

/**
 * This file is part of the "seminars_extbase" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */


/**
 * Speaker
 */
class Speaker extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

/***************************************************
 * Constants
 ***************************************************/

    /**
     * @var int the gender type for speakers without gender
     */
    const GENDER_UNKNOWN = 0;

    /**
     * @var int the gender type male for a speaker
     */
    const GENDER_MALE = 1;

    /**
     * @var int the gender type female for a speaker
     */
    const GENDER_FEMALE = 2;

/***************************************************
 * Database Fields
 ***************************************************/

    /**
     * @var int
     */
    protected $owner;


    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $organization;

    /**
     * @var string
     */
    protected $homepage;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $skills;

    /**
     * @var string
     */
    protected $notes;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $phoneWork;

    /**
     * @var string
     */
    protected $phoneHome;

    /**
     * @var string
     */
    protected $fax;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var int
     */
    protected $gender;

    /**
     * @var int
     */
    protected $cancelationPeriod;


/***************************************************
 * Getters and Setters
 ***************************************************/

    /**
     * @return int
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param int $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param string $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return string
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param string $homepage
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param int $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhoneWork()
    {
        return $this->phoneWork;
    }

    /**
     * @param string $phoneWork
     */
    public function setPhoneWork($phoneWork)
    {
        $this->phoneWork = $phoneWork;
    }

    /**
     * @return string
     */
    public function getPhoneHome()
    {
        return $this->phoneHome;
    }

    /**
     * @param string $phoneHome
     */
    public function setPhoneHome($phoneHome)
    {
        $this->phoneHome = $phoneHome;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return int
     */
    public function getCancelationPeriod()
    {
        return $this->cancelationPeriod;
    }

    /**
     * @param int $cancelationPeriod
     */
    public function setCancelationPeriod($cancelationPeriod)
    {
        $this->cancelationPeriod = $cancelationPeriod;
    }




}
