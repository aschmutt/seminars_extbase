<?php
namespace Schmutt\SeminarsExtbase\Domain\Model;

/**
 * This file is part of the "seminars_extbase" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Event
 */
class Event extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

/***************************************************
 * Constants
 ***************************************************/
    /**
     * @var int represents the type for a single event
     */
    const TYPE_COMPLETE = 0;

    /**
     * @var int represents the type for an event topic
     */
    const TYPE_TOPIC = 1;

    /**
     * @var int represents the type for an event date
     */
    const TYPE_DATE = 2;

    /**
     * @var int the status "planned" for an event
     */
    const STATUS_PLANNED = 0;

    /**
     * @var int the status "canceled" for an event
     */
    const STATUS_CANCELED = 1;

    /**
     * @var int the status "confirmed" for an event
     */
    const STATUS_CONFIRMED = 2;



/***************************************************
 * Database Fields
 ***************************************************/

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $subtitle;

    /**
     * @var string
     */
    protected $teaser;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $topic;

    /**
     * @var int
     */
    protected $eventType;

    /**
     * @var string
     */
    protected $accreditationNumber;

    /**
     * @var int
     */
    protected $creditPoints;

    /**
     * @var int
     */
    protected $categories;

    /**
     * @var int
     */
    protected $beginDate;

    /**
     * @var int
     */
    protected $endDate;

    /**
     * @var string
     */
    protected $timeZone;

    /**
     * @var string
     */
    protected $timeslots;

    /**
     * @var int
     */
    protected $beginDateRegistration;

    /**
     * @var int
     */
    protected $deadlineRegistration;

    /**
     * @var int
     */
    protected $deadlineEarlyBird;

    /**
     * @var int
     */
    protected $deadlineUnregistration;

    /**
     * @var int
     */
    protected $expiry;

    /**
     * @var string
     */
    protected $detailsPage;

    /**
     * @var int
     */
    protected $place;

    /**
     * @var string
     */
    protected $room;

    /**
     * @var int
     */
    protected $lodgings;

    /**
     * @var int
     */
    protected $foods;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Schmutt\SeminarsExtbase\Domain\Model\Speaker>
     */
    protected $speakers;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Schmutt\SeminarsExtbase\Domain\Model\Speaker>
     * @lazy
     */
    protected $partners;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Schmutt\SeminarsExtbase\Domain\Model\Speaker>
     */
    protected $tutors;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Schmutt\SeminarsExtbase\Domain\Model\Speaker>
     */
    protected $leaders;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var double
     */
    protected $priceRegular;

    /**
     * @var double
     */
    protected $priceRegularEarly;

    /**
     * @var double
     */
    protected $priceRegularBoard;

    /**
     * @var double
     */
    protected $priceSpecial;

    /**
     * @var double
     */
    protected $priceSpecialEarly;

    /**
     * @var double
     */
    protected $priceSpecialBoard;

    /**
     * @var string
     */
    protected $additionalInformation;

    /**
     * @var int
     */
    protected $paymentMethods;

    /**
     * @var int
     */
    protected $organizers;

    /**
     * @var int
     */
    protected $organizingPartners;

    /**
     * @var bool
     */
    protected $eventTakesPlaceReminderSent;

    /**
     * @var bool
     */
    protected $cancelationDeadlineReminderSent;

    /**
     * @var bool
     */
    protected $needsRegistration;

    /**
     * @var int
     */
    protected $allowsMultipleRegistrations;

    /**
     * @var int
     */
    protected $attendeesMin;

    /**
     * @var int
     */
    protected $attendeesMax;

    /**
     * @var int
     */
    protected $queueSize;

    /**
     * @var int
     */
    protected $offlineAttendees;

    /**
     * @var int
     */
    protected $targetGroups;

    /**
     * @var bool
     */
    protected $skipCollisionCheck;

    /**
     * @var int
     */
    protected $registrations;

    /**
     * @var bool
     */
    protected $cancelled;

    /**
     * @var int
     */
    protected $ownerFeuser;

    /**
     * @var int
     */
    protected $vips;

    /**
     * @var int
     */
    protected $checkboxes;

    /**
     * @var bool
     */
    protected $usesTerms2;


    /**
     * @var string
     */
    protected $notes;

    /**
     * @var string
     */
    protected $attachedFiles;

    /**
     * @var string
     */
    protected $image;

    /**
     * @var int
     */
    protected $requirements;

    /**
     * @var int
     */
    protected $dependencies;

    /**
     * @var string
     */
    protected $publicationHash;

    /**
     * @var bool
     */
    protected $organizersNotifiedAboutMinimumReached;

    /**
     * @var bool
     */
    protected $muteNotificationEmails;

    /**
     * @var bool
     */
    protected $automaticConfirmationCancelation;

    /**
     * @var bool
     */
    protected $priceOnRequest;

    /**
     * @var int
     */
    protected $dateOfLastRegistrationDigest;


    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $imageFal = NULL;


/***************************************************
 * Fields without database representation
 ***************************************************/

    /**
     * temporary generated detail URL with params and cHash
     * @var string
     */
    protected $detailUri = '';


    /**
     * __construct
     */
    public function __construct() {
        $this->initStorageObjects();
    }

    /**
     * @return void
     */
    protected function initStorageObjects() {
        $this->speakers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->imageFal = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->partners = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->leaders  = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->tutors   = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }


/***************************************************
 * Getters and Setters
 ***************************************************/
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

    /**
     * @param string $teaser
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param int $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return int
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param int $eventType
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * @return string
     */
    public function getAccreditationNumber()
    {
        return $this->accreditationNumber;
    }

    /**
     * @param string $accreditationNumber
     */
    public function setAccreditationNumber($accreditationNumber)
    {
        $this->accreditationNumber = $accreditationNumber;
    }

    /**
     * @return int
     */
    public function getCreditPoints()
    {
        return $this->creditPoints;
    }

    /**
     * @param int $creditPoints
     */
    public function setCreditPoints($creditPoints)
    {
        $this->creditPoints = $creditPoints;
    }

    /**
     * @return int
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param int $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return int
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * @param int $beginDate
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;
    }

    /**
     * @return int
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param int $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param string $timeZone
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }

    /**
     * @return string
     */
    public function getTimeslots()
    {
        return $this->timeslots;
    }

    /**
     * @param string $timeslots
     */
    public function setTimeslots($timeslots)
    {
        $this->timeslots = $timeslots;
    }

    /**
     * @return int
     */
    public function getBeginDateRegistration()
    {
        return $this->beginDateRegistration;
    }

    /**
     * @param int $beginDateRegistration
     */
    public function setBeginDateRegistration($beginDateRegistration)
    {
        $this->beginDateRegistration = $beginDateRegistration;
    }

    /**
     * @return int
     */
    public function getDeadlineRegistration()
    {
        return $this->deadlineRegistration;
    }

    /**
     * @param int $deadlineRegistration
     */
    public function setDeadlineRegistration($deadlineRegistration)
    {
        $this->deadlineRegistration = $deadlineRegistration;
    }

    /**
     * @return int
     */
    public function getDeadlineEarlyBird()
    {
        return $this->deadlineEarlyBird;
    }

    /**
     * @param int $deadlineEarlyBird
     */
    public function setDeadlineEarlyBird($deadlineEarlyBird)
    {
        $this->deadlineEarlyBird = $deadlineEarlyBird;
    }

    /**
     * @return int
     */
    public function getDeadlineUnregistration()
    {
        return $this->deadlineUnregistration;
    }

    /**
     * @param int $deadlineUnregistration
     */
    public function setDeadlineUnregistration($deadlineUnregistration)
    {
        $this->deadlineUnregistration = $deadlineUnregistration;
    }

    /**
     * @return int
     */
    public function getExpiry()
    {
        return $this->expiry;
    }

    /**
     * @param int $expiry
     */
    public function setExpiry($expiry)
    {
        $this->expiry = $expiry;
    }

    /**
     * @return string
     */
    public function getDetailsPage()
    {
        return $this->detailsPage;
    }

    /**
     * @param string $detailsPage
     */
    public function setDetailsPage($detailsPage)
    {
        $this->detailsPage = $detailsPage;
    }

    /**
     * @return int
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param int $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param string $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * @return int
     */
    public function getLodgings()
    {
        return $this->lodgings;
    }

    /**
     * @param int $lodgings
     */
    public function setLodgings($lodgings)
    {
        $this->lodgings = $lodgings;
    }

    /**
     * @return int
     */
    public function getFoods()
    {
        return $this->foods;
    }

    /**
     * @param int $foods
     */
    public function setFoods($foods)
    {
        $this->foods = $foods;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Schmutt\SeminarsExtbase\Domain\Model\Speaker>
     */
    public function getSpeakers()
    {
        return $this->speakers;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Schmutt\SeminarsExtbase\Domain\Model\Speaker>
     */
    public function getPartners()
    {
        return $this->partners;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Schmutt\SeminarsExtbase\Domain\Model\Speaker>
     */
    public function getTutors()
    {
        return $this->tutors;
    }


    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Schmutt\SeminarsExtbase\Domain\Model\Speaker>
     */
    public function getLeaders()
    {
        return $this->leaders;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return float
     */
    public function getPriceRegular()
    {
        return $this->priceRegular;
    }

    /**
     * @param float $priceRegular
     */
    public function setPriceRegular($priceRegular)
    {
        $this->priceRegular = $priceRegular;
    }

    /**
     * @return float
     */
    public function getPriceRegularEarly()
    {
        return $this->priceRegularEarly;
    }

    /**
     * @param float $priceRegularEarly
     */
    public function setPriceRegularEarly($priceRegularEarly)
    {
        $this->priceRegularEarly = $priceRegularEarly;
    }

    /**
     * @return float
     */
    public function getPriceRegularBoard()
    {
        return $this->priceRegularBoard;
    }

    /**
     * @param float $priceRegularBoard
     */
    public function setPriceRegularBoard($priceRegularBoard)
    {
        $this->priceRegularBoard = $priceRegularBoard;
    }

    /**
     * @return float
     */
    public function getPriceSpecial()
    {
        return $this->priceSpecial;
    }

    /**
     * @param float $priceSpecial
     */
    public function setPriceSpecial($priceSpecial)
    {
        $this->priceSpecial = $priceSpecial;
    }

    /**
     * @return float
     */
    public function getPriceSpecialEarly()
    {
        return $this->priceSpecialEarly;
    }

    /**
     * @param float $priceSpecialEarly
     */
    public function setPriceSpecialEarly($priceSpecialEarly)
    {
        $this->priceSpecialEarly = $priceSpecialEarly;
    }

    /**
     * @return float
     */
    public function getPriceSpecialBoard()
    {
        return $this->priceSpecialBoard;
    }

    /**
     * @param float $priceSpecialBoard
     */
    public function setPriceSpecialBoard($priceSpecialBoard)
    {
        $this->priceSpecialBoard = $priceSpecialBoard;
    }

    /**
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * @param string $additionalInformation
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return int
     */
    public function getPaymentMethods()
    {
        return $this->paymentMethods;
    }

    /**
     * @param int $paymentMethods
     */
    public function setPaymentMethods($paymentMethods)
    {
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @return int
     */
    public function getOrganizers()
    {
        return $this->organizers;
    }

    /**
     * @param int $organizers
     */
    public function setOrganizers($organizers)
    {
        $this->organizers = $organizers;
    }

    /**
     * @return int
     */
    public function getOrganizingPartners()
    {
        return $this->organizingPartners;
    }

    /**
     * @param int $organizingPartners
     */
    public function setOrganizingPartners($organizingPartners)
    {
        $this->organizingPartners = $organizingPartners;
    }

    /**
     * @return bool
     */
    public function isEventTakesPlaceReminderSent()
    {
        return $this->eventTakesPlaceReminderSent;
    }

    /**
     * @param bool $eventTakesPlaceReminderSent
     */
    public function setEventTakesPlaceReminderSent($eventTakesPlaceReminderSent)
    {
        $this->eventTakesPlaceReminderSent = $eventTakesPlaceReminderSent;
    }

    /**
     * @return bool
     */
    public function isCancelationDeadlineReminderSent()
    {
        return $this->cancelationDeadlineReminderSent;
    }

    /**
     * @param bool $cancelationDeadlineReminderSent
     */
    public function setCancelationDeadlineReminderSent($cancelationDeadlineReminderSent)
    {
        $this->cancelationDeadlineReminderSent = $cancelationDeadlineReminderSent;
    }

    /**
     * @return bool
     */
    public function isNeedsRegistration()
    {
        return $this->needsRegistration;
    }

    /**
     * @param bool $needsRegistration
     */
    public function setNeedsRegistration($needsRegistration)
    {
        $this->needsRegistration = $needsRegistration;
    }

    /**
     * @return int
     */
    public function getAllowsMultipleRegistrations()
    {
        return $this->allowsMultipleRegistrations;
    }

    /**
     * @param int $allowsMultipleRegistrations
     */
    public function setAllowsMultipleRegistrations($allowsMultipleRegistrations)
    {
        $this->allowsMultipleRegistrations = $allowsMultipleRegistrations;
    }

    /**
     * @return int
     */
    public function getAttendeesMin()
    {
        return $this->attendeesMin;
    }

    /**
     * @param int $attendeesMin
     */
    public function setAttendeesMin($attendeesMin)
    {
        $this->attendeesMin = $attendeesMin;
    }

    /**
     * @return int
     */
    public function getAttendeesMax()
    {
        return $this->attendeesMax;
    }

    /**
     * @param int $attendeesMax
     */
    public function setAttendeesMax($attendeesMax)
    {
        $this->attendeesMax = $attendeesMax;
    }

    /**
     * @return int
     */
    public function getQueueSize()
    {
        return $this->queueSize;
    }

    /**
     * @param int $queueSize
     */
    public function setQueueSize($queueSize)
    {
        $this->queueSize = $queueSize;
    }

    /**
     * @return int
     */
    public function getOfflineAttendees()
    {
        return $this->offlineAttendees;
    }

    /**
     * @param int $offlineAttendees
     */
    public function setOfflineAttendees($offlineAttendees)
    {
        $this->offlineAttendees = $offlineAttendees;
    }

    /**
     * @return int
     */
    public function getTargetGroups()
    {
        return $this->targetGroups;
    }

    /**
     * @param int $targetGroups
     */
    public function setTargetGroups($targetGroups)
    {
        $this->targetGroups = $targetGroups;
    }

    /**
     * @return bool
     */
    public function isSkipCollisionCheck()
    {
        return $this->skipCollisionCheck;
    }

    /**
     * @param bool $skipCollisionCheck
     */
    public function setSkipCollisionCheck($skipCollisionCheck)
    {
        $this->skipCollisionCheck = $skipCollisionCheck;
    }

    /**
     * @return int
     */
    public function getRegistrations()
    {
        return $this->registrations;
    }

    /**
     * @param int $registrations
     */
    public function setRegistrations($registrations)
    {
        $this->registrations = $registrations;
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return $this->cancelled;
    }

    /**
     * @param bool $cancelled
     */
    public function setCancelled($cancelled)
    {
        $this->cancelled = $cancelled;
    }

    /**
     * @return int
     */
    public function getOwnerFeuser()
    {
        return $this->ownerFeuser;
    }

    /**
     * @param int $ownerFeuser
     */
    public function setOwnerFeuser($ownerFeuser)
    {
        $this->ownerFeuser = $ownerFeuser;
    }

    /**
     * @return int
     */
    public function getVips()
    {
        return $this->vips;
    }

    /**
     * @param int $vips
     */
    public function setVips($vips)
    {
        $this->vips = $vips;
    }

    /**
     * @return int
     */
    public function getCheckboxes()
    {
        return $this->checkboxes;
    }

    /**
     * @param int $checkboxes
     */
    public function setCheckboxes($checkboxes)
    {
        $this->checkboxes = $checkboxes;
    }

    /**
     * @return bool
     */
    public function isUsesTerms2()
    {
        return $this->usesTerms2;
    }

    /**
     * @param bool $usesTerms2
     */
    public function setUsesTerms2($usesTerms2)
    {
        $this->usesTerms2 = $usesTerms2;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return string
     */
    public function getAttachedFiles()
    {
        return $this->attachedFiles;
    }

    /**
     * @param string $attachedFiles
     */
    public function setAttachedFiles($attachedFiles)
    {
        $this->attachedFiles = $attachedFiles;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @param int $requirements
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;
    }

    /**
     * @return int
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }

    /**
     * @param int $dependencies
     */
    public function setDependencies($dependencies)
    {
        $this->dependencies = $dependencies;
    }

    /**
     * @return string
     */
    public function getPublicationHash()
    {
        return $this->publicationHash;
    }

    /**
     * @param string $publicationHash
     */
    public function setPublicationHash($publicationHash)
    {
        $this->publicationHash = $publicationHash;
    }

    /**
     * @return bool
     */
    public function isOrganizersNotifiedAboutMinimumReached()
    {
        return $this->organizersNotifiedAboutMinimumReached;
    }

    /**
     * @param bool $organizersNotifiedAboutMinimumReached
     */
    public function setOrganizersNotifiedAboutMinimumReached($organizersNotifiedAboutMinimumReached)
    {
        $this->organizersNotifiedAboutMinimumReached = $organizersNotifiedAboutMinimumReached;
    }

    /**
     * @return bool
     */
    public function isMuteNotificationEmails()
    {
        return $this->muteNotificationEmails;
    }

    /**
     * @param bool $muteNotificationEmails
     */
    public function setMuteNotificationEmails($muteNotificationEmails)
    {
        $this->muteNotificationEmails = $muteNotificationEmails;
    }

    /**
     * @return bool
     */
    public function isAutomaticConfirmationCancelation()
    {
        return $this->automaticConfirmationCancelation;
    }

    /**
     * @param bool $automaticConfirmationCancelation
     */
    public function setAutomaticConfirmationCancelation($automaticConfirmationCancelation)
    {
        $this->automaticConfirmationCancelation = $automaticConfirmationCancelation;
    }

    /**
     * @return bool
     */
    public function isPriceOnRequest()
    {
        return $this->priceOnRequest;
    }

    /**
     * @param bool $priceOnRequest
     */
    public function setPriceOnRequest($priceOnRequest)
    {
        $this->priceOnRequest = $priceOnRequest;
    }

    /**
     * @return int
     */
    public function getDateOfLastRegistrationDigest()
    {
        return $this->dateOfLastRegistrationDigest;
    }

    /**
     * @param int $dateOfLastRegistrationDigest
     */
    public function setDateOfLastRegistrationDigest($dateOfLastRegistrationDigest)
    {
        $this->dateOfLastRegistrationDigest = $dateOfLastRegistrationDigest;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    public function getImageFal()
    {
        return $this->imageFal;
    }

    /**
     * @return string
     */
    public function getDetailUri()
    {
        return $this->detailUri;
    }

    /**
     * @param string $detailUri
     */
    public function setDetailUri($detailUri)
    {
        $this->detailUri = $detailUri;
    }


}