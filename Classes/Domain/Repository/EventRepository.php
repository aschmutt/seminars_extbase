<?php

namespace Schmutt\SeminarsExtbase\Domain\Repository;

/**
 * This file is part of the "seminars_extbase" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class EventRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
    * get List of Events
    */
    public function getEventList()
    {
        /**@var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder $uriBuilder */
        $uriBuilder = $this->objectManager->get(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder::class);

        $query = $this->createQuery();
        /*$query->matching(
            $query->logicalAnd(
                $query->equals("mySpecialProperty", "isMySpecialValue"),
                $query->equals("someOtherProperty", "isAnotherValue")
            )
        );*/
        $result = $query->execute();

        /**@var \Schmutt\SeminarsExtbase\Domain\Model\Event $event */
        foreach ($result as $event) {
            $detailsPage = 0;
            if (strlen($event->getDetailsPage()) > 0) {
                $detailsPage = $this->getPageUidFromTypolink($event->getDetailsPage());

            }
            //@todo: get plugin settings
            if ($detailsPage == 0) {
                $detailsPage = $GLOBALS['TSFE']->id;
                $event->setDetailsPage('t3://page?uid=' . $detailsPage);
            }


            $pluginArgs = [
                'event' => $event->getUid()
            ];
            $uri = $uriBuilder
                ->reset()
                ->setTargetPageUid($detailsPage)
                ->uriFor('show', $pluginArgs, 'Seminar', 'seminarsextbase', 'Pi1');
            $event->setDetailUri($uri);
        }
        return $result;

    }





    /**
    * @var string $typolink
     * @return int
     */
    public function getPageUidFromTypolink($typolink) {
        if (strpos($typolink,'t3://page?uid=') === 0) {
            $data = explode('=', $typolink);
            $pageUid = (int)$data[1];
        }

        return $pageUid;
    }
}
