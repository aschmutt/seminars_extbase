<?php

namespace Schmutt\SeminarsExtbase\Domain\Repository;

/**
 * This file is part of the "seminars_extbase" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class SpeakerRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

}
