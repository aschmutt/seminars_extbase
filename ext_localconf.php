<?php
defined('TYPO3_MODE') or die();

$boot = function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Schmutt.seminars_extbase',
        'Pi1',
        [
            'Seminar' => 'list,show'
        ],
        // non-cacheable actions
        [
            'Seminar' => ''
        ]
    );

};

$boot();
unset($boot);

