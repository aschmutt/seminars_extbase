##TYPO3 Extension "seminars_extbase"

This is the Update project for the Extension "seminars"

Until the update is finished, there will be both extensions available for parallel use. 

###Installation: 
* install EXT:seminars, add Include Static Template
* install EXT:seminars_extbase, add Include Static Template AFTER seminars
* List TCA will have "Additional Fields" Tab from this extension


###Update Philosophy:

The main goals are: 
* update code to current TYPO3 standards: Extbase MVC structure, Fluid templates, FAL  
* remove extension dependencies and conflicts
* backward compatibility and update options for EXT:seminars
* parallel use: updated features can be used from EXT:seminars_extbase", old features still work with EXT:seminars 

###Roadmap
#####Milestone 1: Standalone Version:  
 
EXT:seminars_extbase runs without EXT:seminars with limited features. 
New Installations can start without legacy code.
For all features, a parallel installation is possible.

* Frontend as Fluid Template: List view, Detail view, Categories
* Model and Repository functions for all tables, TCA
* adding Plugin settings (Flexform, TS)

#####Milestone 2: Registration 
Registration and Event management runs without EXT:seminars

* Registration for Events
* Mailing as Fluid Templates, Notification Settings
* Backend Module: Registration Management, CSV Export

#####Feature and Wish List
* My Events: FE User connection to events and registrations
* Frontend Management for Events
* Full replacement of EXT:seminar with upgrade script
* DSGVO: Delete Scheduler Task

####Reference

* https://extensions.typo3.org/extension/seminars/
* https://github.com/oliverklee/ext-seminars