<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

// Configure new fields:
$fields = array(
    'image_fal' => [
        'label' => 'LLL:EXT:seminars_extbase/Resources/Private/Language/locallang_db.xlf:tx_seminarsextbase_seminars.image_fal',
        'exclude' => 1,
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'sys_file_reference',
            'foreign_field' => 'uid_foreign',
            'foreign_sortby' => 'sorting_foreign',
            'foreign_table_field' => 'tablenames',
            'foreign_match_fields' => [
                'fieldname' => 'image_fal',
            ],
            'foreign_label' => 'uid_local',
            'foreign_selector' => 'uid_local',
            'minitems' => 0,
            'maxitems' => 99,
            'overrideChildTca' => [
                'columns' =>
                    array (
                        'uid_local' =>
                            array (
                                'config' =>
                                    array (
                                        'appearance' =>
                                            array (
                                                'elementBrowserType' => 'file',
                                                'elementBrowserAllowed' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,svg',
                                            ),
                                    ),
                            ),
                    ),
                'types' =>
                    array (
                        0 =>
                            array (
                                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ),
                        1 =>
                            array (
                                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ),
                        2 =>
                            array (
                                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ),
                        3 =>
                            array (
                                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ),
                        4 =>
                            array (
                                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ),
                        5 =>
                            array (
                                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ),
                    ),
            ],
        ],
     ],
);

// Add new fields to pages:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_seminars_seminars', $fields);

// Make fields visible in the TCEforms:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tx_seminars_seminars', // Table name
    '--div--;LLL:EXT:seminars_extbase/Resources/Private/Language/locallang_db:tx_seminarsextbase_seminars.div_title, image_fal',
    '',
    'after:vips'
);

