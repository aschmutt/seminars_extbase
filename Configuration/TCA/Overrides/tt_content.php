
<?php
if (!defined('TYPO3_MODE')) {die('Access denied.');}

/***************
 * Plugin
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'seminars_extbase',
    'Pi1',
    'Seminars (Extbase)'
);


/************** Flexform Setup **********************/
$pluginSignature = 'seminarsextbase_pi1';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:seminars_extbase/Configuration/FlexForms/flexform.xml');
