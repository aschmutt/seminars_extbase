<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "seminars".
 *
 * Auto generated 29-09-2018 17:05
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Seminar Manager (Extbase)',
  'description' => 'Allows you to create and manage a list of seminars, workshops, lectures, theater performances and other events, allowing front-end users to sign up. FE users also can create and edit events.',
  'category' => 'plugin',
  'state' => 'stable',
  'createDirs' => 'uploads/tx_seminars/',
  'clearCacheOnLoad' => 1,
  'author' => 'Andrea Schmuttermair',
  'author_email' => 'andrea@schmutt.de',
  'author_company' => 'schmutt.de',
  'version' => '1.0.0',
  'constraints' => 
  array (
    'depends' => 
    array (
      'php' => '5.6.0-7.2.99',
      'typo3' => '7.6.0-8.7.99',
    ),
  ),
  'autoload' => 
  array (
    'classmap' => 
    array (
      0 => 'Classes',
    ),
  ),
  'uploadfolder' => true,
  'clearcacheonload' => true,
);

